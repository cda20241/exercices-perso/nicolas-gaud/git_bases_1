def addition(x, y):
    # Cette fonction prend en argument deux entiers
    # et retourne le résultat de leur addition
    result = x + y
    return result


print(addition(10, 1))

# Pour conflit
print("Hello")
